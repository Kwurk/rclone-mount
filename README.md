# rclone-mount
Wrapper script for managing multiple Rclone mounts with Systemd

## Requirements
- Rclone
- Systemd
- `mounts.conf` under `~/.config/rclone`
- Ability to copy-paste commands

## Installation

1. Clone and `cd` into the repo
2. Run `sed -i "s|<USERNAME>|$(whoami)|" rclone-vfs@.service`
3. Copy `rclone-vfs@.service` to: `/etc/systemd/system`
- This script uses `Mounts` folder under your home directory to mount all remotes, change it to your needs
4. `chmod +x mount-helper`

## Usage

1. `./mount-helper`
- Follow the instructions

### One-liner (global installation)

```
curl -s https://gitlab.com/Kwurk/rclone-mount/-/raw/main/mount-helper > /usr/local/bin/mount-helper &&
chmod +x /usr/local/bin/mount-helper &&
curl -s https://gitlab.com/Kwurk/rclone-mount/-/raw/main/rclone-vfs@.service | sed "s|<USERNAME>|$(whoami)|" > /etc/systemd/system/rclone-vfs@.service
```
- Run with `mount-helper`
